<?php include './navbar.php';?>
<?php
ob_start();
?>

<html>
<head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

    <title></title></head>
<link rel="stylesheet" type="text/css" href="style.css">
<body>
<script language="javascript">
    function confirm_delete()
    {
        window.location='propertyModify.php?id=<?php echo $_GET["id"]; ?>&Action=ConfirmDelete';
    }
</script>
<center><h3>property Modification</h3></center>
<?php
include("connection.php");
$connect = new mysqli($host, $user, $password, $database)
or die("Couldn't log on to database");

$query = "SELECT * FROM property WHERE id =".$_GET["id"];
$result = $connect->query(($query));
$row = $result->fetch_assoc();

$strAction = $_GET["Action"];

switch($strAction)
{
case "Update":
    ?>
    <form method="post" action="propertyModify.php?id=<?php echo $_GET["id"]; ?>&Action=ConfirmUpdate">
        <center>property details amendment<br /></center><p />
        <table align="center" cellpadding="3">
            <tr />
            <td><b>Property ID</b></td>
            <td><?php echo $row["id"]; ?></td>
            </tr>
            <tr>
                <td><b>Client ID</b></td>
                <td> <select name="client_id" required>
                        <?php
                        $query ="SELECT * FROM client ORDER BY ID DESC";
                        $result = mysqli_query($connect, $query);


                        while($row = mysqli_fetch_array($result))
                        {
                        ?>	<option value="<?php echo $row["id"]?>"> <?php echo $row["id"];echo ", ";  echo $row["client_gname"]; echo ", "; echo $row["client_fname"];
                            }
                            ?>
                        </option>
                   </tr>
            <tr>
                <td><b>Property Street</b></td>
                <td><input type="text" name="propertystreet" size="30" value="<?php echo $row["property_street"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Property Suburb</b></td>
                <td><input type="text" name="propertysuburb" size="40" value="<?php echo $row["property_suburb"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Property PostCode</b></td>
                <td><input type="text" name="propertypc" size="10" value="<?php echo $row["property_pc"]; ?>"></td>
            </tr>
            <tr>
                <td><b>List Price</b></td>
                <td><input type="text" name="listprice" size="10" value="<?php echo $row["list_price"]; ?>"></td>
            </tr>
            <tr>
                <td><b>List Date</b></td>
                <td><input type="date" name="listdate" size="10" value="<?php echo $row["list_date"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Sale Date</b></td>
                <td><input type="date" name="saledate" size="10" value="<?php echo $row["sale_date"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Sales Price</b></td>
                <td><input type="text" name="salesprice" size="10" value="<?php echo $row["sales_price"]; ?>"></td>
            </tr>
            
            <tr>
                <td><b>Property Type</b></td>
                <td> <select name="Type " required>
                        <?php
                        $query ="SELECT * FROM type ORDER BY ID DESC";
                        $result = mysqli_query($connect, $query);


                        while($row = mysqli_fetch_array($result))
                        {
                        ?>	<option value="<?php echo $row["id"]?>"> <?php echo $row["id"];echo ", ";  echo $row["type_name"];
                            }
                            ?>
                        </option>
            </tr>
            <tr>
                <td>Property picture</td>
                <td> <input type="file" name="image_name"> (Please upload a single picture)</td>
            </tr>
            <tr>
                <td><b>Description</b></td>
                <td><input type="text" name="descri" size="50" value="<?php echo $row["description"]; ?>"></td>
            </tr>
        </table>
        <br/>
        <table align="center">
            <tr>
                <td><input type="submit" value="Update property"></td>
                <td><input type="button" value="Return to List" OnClick="window.location='view_property.php'"></td>
            </tr>
        </table>
    </form>
    <?php
    break;

case "ConfirmUpdate":
{
$query = "UPDATE property set client_id ='$_POST[clientid]',
	                property_street='$_POST[propertystreet]', property_suburb='$_POST[propertysuburb]',
	                property_pc='$_POST[propertypc]',list_price='$_POST[listprice]',
	                list_date='$_POST[listdate]', sale_date='$_POST[saledate]',
	                sales_price='$_POST[salesprice]',sales_price='$_POST[salesprice]',
	                property_type='$_POST[propertytype]',
	                description='$_POST[descri]'     

	                 WHERE id =" .$_GET["id"];
        $result = $connect->query(($query));



if ($connect->query($query))
{
?>
<center>
    The following property record has been successfully update<p/>
    <?php
    echo "property No. $row[id] $row[client_id] ";
    echo "</center><p />";
    echo "<center><a href='view_property.php'><h4 class='alert_success'>The property details has successfully added</h4></a></center>";

    }


    else {
        echo "<center>Error updatinng property record<p /></center>";
    }

    }
    break;



case "Delete":
    ?>
    <center>Confirm deletion of the following property record<br /></center><p />
    <table align="center" cellpadding="3">
        <tr />
        <td><b>Property ID</b></td>
        <td><?php echo $row["id"]; ?></td>
        </tr>
        <tr>
            <td><b>Client ID</b></td>
            <td><?php echo $row["client_id"]; ?></td>
        </tr>
        <tr>
            <td><b>Property Street</b></td>
            <td><?php echo $row["property_street"]; ?></td>
        </tr>
        <tr>
            <td><b>Property Suburb</b></td>
            <td><?php echo $row["property_suburb"]; ?></td>
        </tr>
        <tr>
            <td><b>Property_State</b></td>
            <td><?php echo $row["property_pc"]; ?></td>
        </tr>
        <tr>
            <td><b>List Price</b></td>
            <td><?php echo $row["list_price"]; ?></td>
        </tr>
        <tr>
            <td><b>List Date</b></td>
            <td><?php echo $row["list_date"]; ?></td>
        </tr>
        <tr>
            <td><b>Sale Date</b></td>
            <td><?php echo $row["sale_date"]; ?></td>
        </tr>
        <tr>
            <td><b>Sales Price</b></td>
            <td><?php echo $row["sales_price"]; ?></td>
        </tr>
        <tr>
            <td><b>Property Type</b></td>
            <td><?php echo $row["property_type"]; ?></td>
        </tr>
        <tr>
            <td><b>Description</b></td>
            <td><?php echo $row["description"]; ?></td>
        </tr>




    </table>
    <br/>
    <table align="center">
        <tr>
            <td><input type="button" value="Confirm" OnClick="confirm_delete();">
            <td><input type="button" value="Cancel" OnClick="window.location='view_property.php'"></td>
        </tr>
    </table>
    <?php
    break;

case "ConfirmDelete":
$query="DELETE FROM property WHERE id =".$_GET["id"];
if($connect->query($query))
{
?>
<center>
    The following property record has been successfully deleted<p />
    <?php
    echo "property No. $row[id] $row[client_id] ";
    echo "</center><p />";
    }
    else
    {
        echo "<center>Error deleting property record<p /></center>";
    }
    echo "<center><input type='button' value='Return to List' OnClick='window.location=\"view_property.php\"'></center>";
    break;
    }
    $connect->close();
    ?>
</body>
</html>













