<?php include './logincheck.php';?>
<?php include './navbar.php';?>
<?php require './connection.php';

?>
<?php ?>

<?php
$connect = mysqli_connect("$host", "$user", "$password", "$database");
$query ="SELECT * FROM client ORDER BY ID DESC";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
</head>
<body>
<br /><br />
<div class="container">
    <h3 align="center">Ruthless Real Estate clients</h3>
    <input type='button' value='View PDF' OnClick='window.location="clientPDF.php"'>
    <br />
    <div class="table-responsive">
        <table id="client_data" class="table table-striped table-bordered">
            <thead>
            <tr>

                <td>Id</td>
                <td>Given Name</td>
                <td>Family Name</td>
                <td>Street</td>
                <td>Suburb</td>
                <td>State</td>
                <td>Postcode</td>
                <td>Email</td>
                <td>Mobile Phone</td>
                <td>Mailing List</td>
                <td>Actions</td>


            </tr>
            </thead>
            <?php
            while($row = mysqli_fetch_array($result))
            {
                ?>
                               <tr>  


                                    <td><?php echo$row["id"] ?></td>
                                    <td><?php echo$row["client_gname"]?></td>
                                    <td><?php echo$row["client_fname"]?></td>
                                    <td><?php echo$row["client_street"]?></td>
                                    <td><?php echo$row["client_suburb"]?></td>
                                    <td><?php echo$row["client_state"]?></td>
                                    <td><?php echo$row["client_pc"]?></td>
                                    <td> <a href=\"mailto:"> <?php echo$row["client_email"]?></a></td>
                                    <td><?php echo$row["client_mobile"]?></td>
                                    <td><?php echo$row["client_mailinglist"]?></td>
                                    <td> <a href="./clientModify.php?Action=Update&id=<?php echo $row["id"];?>">Update</a>
                                         <a href="./clientModify.php?Action=Delete&id=<?php echo $row["id"];?>">Delete</a></td>
                                    <form method="post" action="./clientMail.php" >
       
                                    <td><input type="checkbox" id ="email" name="check[]" value="<?php echo $row["client_email"]; ?> required"></td>
       
                               </tr>  
            <?php
            }
            ?>
        </table>
        <input type="submit" value="Send Emails">
    </form>

    </div>
</div>
</body>
<footer>
    <center>
        <a   target='_blank' href="./displayCode.php?name=view_client.php"><img width="500" height="50" src="./images/client.PNG"></a>
    </center>
</footer>

</html>
<script>
    $(document).ready(function(){
        $('#client_data').DataTable();
    });
</script>
