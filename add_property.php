<?php include './logincheck.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
</head>
<?php include './navbar.php';
require 'connection.php';
$connect = mysqli_connect("$host", "$user", "$password", "$database");
$query ="SELECT * FROM property ORDER BY ID DESC";
$result = mysqli_query($connect, $query)
?>
<body>
<table border="0" style="border-style:double">
    <center><h1>Property Details</h1></center>
    <form method="post" action="property_success.php">
        <tr>
            <td> Client </td>
            <td> <select name="client_id" required>
                    <?php
                    $query ="SELECT * FROM client ORDER BY ID DESC";
                    $result = mysqli_query($connect, $query);


                    while($row = mysqli_fetch_array($result))
                    {
                    ?>	<option value="<?php echo $row["id"]?>"> <?php echo $row["client_gname"]; echo ", "; echo $row["client_fname"];
                        }
                        ?>
                    </option>
        </tr>
        </tr>
        <tr>
            <td>Street Address:</td>
            <td><input type="text" name="property_street" size="35" maxlength="50" placeholder="21 Clayton Road" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('your address is required, please put a valid street address.')" required autofocus></td>
            <td>Suburb:</td>
            <td><input type="text" name="property_suburb" size="35" maxlength="50" placeholder="Clayton" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('The suburb character is longer .')" > </td>
        </tr>
        <tr>
            <td>State:</td>
            <td><select name="property_state">
                    <option></option>
                    <option>VIC</option>
                    <option>SA</option>
                    <option>TAS</option>
                    <option>WA</option>
                    <option>QLD</option>
                    <option>ACT</option>
                    <option>NSW</option>
                    <option>NT</option>
                </select></td>
            <td>Postcode:</td>
            <td><input type="number" name="property_pc" size=4 min="0" max="9999" maxlength="4" placeholder="3000"  onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('Postcode cannot be longer than 4 numbers')" ></td>
        </tr>

        <tr>
            <td> List Price </td>
            <td> <input type="number" name="list_price" size="10" maxlength="10" placeholder="$100,000"  onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('Please put a listing price for the property')"> </td>
            <td>List date</td>
            <td><input type="datetime-local" name="list_date" </td>
        </tr>

        <tr>
            <td> Sale Price </td>
            <td> <input type="number" name="sales_price" size="10" maxlength="10" placeholder="$100,000"  onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('Please put a price for the property')"> </td>
            <td>sale date</td>
            <td><input type="datetime-local" name="sale_date" </td>
        </tr>

        <tr>
            <td> Type </td>
            <td> <select name="property_type" required>
                    <?php
                    $query ="SELECT * FROM type ORDER BY ID DESC";
                    $result = mysqli_query($connect, $query);


                    while($row = mysqli_fetch_array($result))
                    {
                    ?>	<option value="<?php echo $row["id"]?>"> <?php echo $row["type_name"];
                        }
                        ?>
                    </option>
            </td>
        </tr>

        <tr>
            <td>Property picture</td>
            <td> <input type="file" name="image_name"> (Please upload a single picture)</td>
        </tr>

        <tr>
            <td> Description</td>
            <td><input type="text" name="descriptions" size="60" maxlength="255" placeholder="Write a brief description of the property"  ></td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="Submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Reset"> <input type="submit" OnClick='window.history.back()'value="Back"</></td>
        </tr>

    </form>
</table>


</body>
</html>