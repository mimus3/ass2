<?php include './logincheck.php';?>
<?php include './navbar.php';?>
<?php require './connection.php';?>

<?php
$connect = mysqli_connect("$host", "$user", "$password", "$database");
$query ="SELECT * FROM feature ORDER BY ID DESC";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
</head>
<body>
<br /><br />
<div class="container">
    <h3 align="center">Property features</h3>
    <br />
    <div class="table-responsive">
        <table id="property_data" class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>id</td>
                <td>Feature Name</td>
                <td>Actions</td>
            </tr>
            </thead>
            <?php
            while($row = mysqli_fetch_array($result))
            {

                ?>
            <tr>
                                    <td><?php echo $row["id"]?></td>
                                    <td><?php echo$row["feature_name"]?></td>
                                    <td> <a href="./featureModify.php?Action=Update&id=<?php echo $row["id"];?>"> Update</a>
                                         <a href="./featureModify.php?Action=Delete&id=<?php echo $row["id"];?> ">Delete</a></td>
                               </tr>
            <?php

            }
            ?>
        </table>
    </div>
</div>
</body>
</html>
<script>
    $(document).ready(function(){
        $('#property_data').DataTable();
    });
</script>

<footer>
    <center>
        <a   target='_blank' href="./displayCode.php?name=view_propertyfeature.php"><img width="500" height="50" src="./images/feature.PNG"></a>
    </center>
</footer>