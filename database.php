<?php include './navbar.php' ?>
<h1>Create Table statement</h1>
<pre>

create table authenticate
(
id int auto_increment
primary key,
uname varchar(50) not null,
pword varchar(64) not null,
given_name varchar(50) not null,
family_name varchar(50) not null
)
;

create table client
(
id int auto_increment
primary key,
client_gname varchar(50) not null,
client_fname varchar(50) not null,
client_street varchar(100) null,
client_suburb varchar(50) null,
client_state varchar(6) null,
client_pc int(4) null,
client_email varchar(50) null,
client_mobile varchar(20) null,
client_mailinglist tinyint(1) null
)
;

create table feature
(
id int auto_increment
primary key,
feature_name varchar(30) not null
)
;

create table property
(
id int auto_increment
primary key,
client_id int not null,
property_street varchar(100) not null,
property_suburb varchar(50) not null,
property_state varchar(6) not null,
property_pc int(4) not null,
list_price varchar(20) not null,
list_date datetime not null,
sale_date datetime null,
sales_price varchar(20) null,
property_type varchar(50) not null,
image_name varchar(300) null,
description varchar(255) null,
constraint property_client_id_fk
foreign key (client_id) references client (id)
)
;

create index property_client_id_fk
on property (client_id)
;

create index property_type_id_fk
on property (property_type)
;

create table propertyfeature
(
property_id int not null,
feature_id int not null,
feature_desc varchar(50) not null,
primary key (property_id, feature_id),
constraint propertyfeature_property_id_fk
foreign key (property_id) references property (id),
constraint propertyfeature_feature_id_fk
foreign key (feature_id) references feature (id)
)
;

create index propertyfeature_feature_id_fk
on propertyfeature (feature_id)
;

create table type
(
id int auto_increment
primary key,
type_name varchar(300) not null
)
;

</pre>