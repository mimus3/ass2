</head>
<?php include './navbar.php';?>

<!DOCTYPE html>
<html>

<style>
    table {
        color: #333;
        font-family: Helvetica, Arial, sans-serif;
        width: 640px;
        border-collapse:
            collapse; border-spacing: 0;
    }

    td, th {
        border: 1px solid transparent; /* No more visible border */
        height: 30px;
        transition: all 0.3s;  /* Simple transition for hover effect */
    }

    th {
        background: #DFDFDF;  /* Darken header a bit */
        font-weight: bold;
    }

    td {
        background: #FAFAFA;
        text-align: center;
    }

    /* Cells in even rows (2,4,6...) are one color */
    tr:nth-child(even) td { background: #F1F1F1; }

    /* Cells in odd rows (1,3,5...) are another (excludes header cells)  */
    tr:nth-child(odd) td { background: #FEFEFE; }

    tr td:hover { background: #666; color: #FFF; }
    /* Hover cell effect! */
    }</style>

<table><thead>
    <tr>
        <th>Parts</th>
        <th>Mohammed Imam Mustapha - 24862320</th>
        <th>Detao Zhang</th>
    </tr>
    <tr>
        <th>Main Page</th>
        <th>40%</th>
        <th>60%</th>
    </tr>
    <tr>
        <th>View Property Page</th>
        <th>70%</th>
        <th>30%</th>
    </tr>

    <tr>
        <th>add Property Page</th>
        <th>80%</th>
        <th>20%</th>
    </tr>

    <tr>
        <th>Edit/Update Property Page</th>
        <th>20%</th>
        <th>80%</th>
    </tr>
    <tr>
        <th>search Property Page</th>
        <th>90%</th>
        <th>10%</th>
    </tr>

    <tr>
        <th>View Client Pages</th>
        <th>75%</th>
        <th>25%</th>
    </tr>
    <tr>
        <th>Add Client Pages</th>
        <th>90%</th>
        <th>10%</th>
    </tr>
    <tr>
        <th>Update/Edit Client Pages</th>
        <th>30%</th>
        <th>70%</th>
    </tr>

    <tr>
        <th>Email Client Pages</th>
        <th>100%</th>
        <th>0%</th>
    </tr>

    <tr>
        <th>View Type Pages</th>
        <th>70%</th>
        <th>30%</th>
    </tr>

    <tr>
        <th>Add Type Pages</th>
        <th>90%</th>
        <th>10%</th>
    </tr>

    <tr>
        <th>Update/Edit type Pages</th>
        <th>40%</th>
        <th>60%</th>
    </tr>

    <tr>
        <th>View feature Pages</th>
        <th>90%</th>
        <th>10%</th>
    </tr>

    <tr>
        <th>Add feature Pages</th>
        <th>90%</th>
        <th>10%</th>
    </tr>

    <tr>
        <th>Update/Edit type Pages</th>
        <th>40%</th>
        <th>60%</th>
    </tr>
    <tr>
        <th>Images Design and Functions</th>
        <th>85%</th>
        <th>15%</th>
    </tr>
    <tr>
        <th>Security</th>
        <th>25%</th>
        <th>75%</th>
    </tr>

    <tr>
        <th>Multiple Property</th>
        <th>50%</th>
        <th>50%</th>
    </tr>
    </thead>