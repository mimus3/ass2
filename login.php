<?php
session_start();

include("connection.php");
//use the variable names in the include file
$connect = new mysqli($host, $user, $password, $database);

$error = false;
if(isset($_POST['btn-login'])){
    $username = trim($_POST['username']);
    $username = htmlspecialchars(strip_tags($username));

    $password = trim($_POST['password']);
    $password = htmlspecialchars(strip_tags($password));

    if(empty($username)){
        $error = true;
        $errorUsername = 'Please input username';
    }
//elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
//        $error = true;
//        $errorEmail = 'Please enter a valid email address';
//    }

    if(empty($password)){
        $error = true;
        $errorPassword = 'Please enter password';
    }
//    elseif(strlen($password)< 6){
//        $error = true;
//        $errorPassword = 'Password at least 6 character';
//    }

    if(!$error){
        $password = md5($password);
        $sql = "select * from authenticate where username='$username'";
        $result = mysqli_query($connect, $sql);
        $count = mysqli_num_rows($result);
        $row = mysqli_fetch_assoc($result);
        if($count==1 && $row['password'] == $password){
            $_SESSION['username'] = $row['username'];
            header('location: index.php');
        }else{
            $errorMsg = 'Invalid Username or Password';
        }
    }
}

?>

<html>
<head>
    <title>PHP Login & Register</title>
    <link rel="stylesheet" href="loginAsset/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div style="width: 500px; margin: 50px auto;">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
            <center><h2>Login</h2></center>
            <hr/>
            <?php
            if(isset($errorMsg)){
                ?>
                <div class="alert alert-danger">
                    <span class="glyphicon glyphicon-info-sign"></span>
                    <?php echo $errorMsg; ?>
                </div>
                <?php
            }
            ?>
            <!--            <div class="form-group">-->
            <!--                <label for="email" class="control-label">Email</label>-->
            <!--                <input type="email" name="email" class="form-control" autocomplete="off">-->
            <!--                <span class="text-danger">--><?php //if(isset($errorEmail)) echo $errorEmail; ?><!--</span>-->
            <!--            </div>            -->
            <div class="form-group">
                <label for="username" class="control-label">Username</label>
                <input type="username" name="username" class="form-control" autocomplete="off">
                <span class="text-danger"><?php if(isset($errorUsername)) echo $errorUsername; ?></span>
            </div>
            <div class="form-group">
                <label for="password" class="control-label">Password</label>
                <input type="password" name="password" class="form-control" autocomplete="off">
                <span class="text-danger"><?php if(isset($errorPassword)) echo $errorPassword; ?></span>
            </div>
            <div class="form-group">
                <center><input type="submit" name="btn-login" value="Login" class="btn btn-primary"></center>
            </div>

            <hr/>
            <!--            <a href="register.php">Register</a>-->
        </form>
    </div>
</div>
</body>
</html>