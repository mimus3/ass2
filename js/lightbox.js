window.onload = setupLightbox;


var lightboxOverlay;
var lightboxImage;
var lightboxCloseBtn;



function setupLightbox() {
  for ( i in document.links ) {
    if ( document.links[ i ].rel == "lightbox" ) {
      document.links[ i ].onclick = showLightbox;
    }
  }
}


function showLightbox() {
  lightboxOverlay = document.createElement( "div" );
  lightboxImage = document.createElement( "img" );
  lightboxCloseBtn = document.createElement( "a" );

  

  lightboxOverlay.id = "lightboxOverlay";
  document.body.appendChild( lightboxOverlay );
  
  lightboxOverlay.onclick = closeLightbox;
  lightboxCloseBtn.onclick = closeLightbox;
  lightboxImage.onload = showImage;
  
  lightboxImage.src = this.href;
  lightboxImage.title = this.title;
  return false;
}


function showImage() {
  lightboxImage.id = "lightboxImage";
  lightboxImage.style.marginLeft = -lightboxImage.width / 2 - 10 + "px";
  lightboxImage.style.marginTop = -lightboxImage.height / 2 - 10 + "px";
  document.body.appendChild( lightboxImage );
  
  lightboxCloseBtn.id = "lightboxCloseBtn";
  lightboxCloseBtn.innerHTML = "x";
  lightboxCloseBtn.style.marginLeft = lightboxImage.width / 2 - 15 + "px";
  lightboxCloseBtn.style.marginTop = -lightboxImage.height / 2  + "px";
  document.body.appendChild( lightboxCloseBtn );
  
}


function closeLightbox() {
  lightboxImage.style.opacity = lightboxOverlay.style.opacity = "0";
  setTimeout( function() {
    lightboxImage.parentNode.removeChild(lightboxImage);
    lightboxOverlay.parentNode.removeChild(lightboxOverlay);
    lightboxCloseBtn.parentNode.removeChild(lightboxCloseBtn);
  }, 1);
}