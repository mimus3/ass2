
<?php include './logincheck.php';?>
<?php include './navbar.php';?>
<?php require './connection.php';?>


<?php
$connect = mysqli_connect("$host", "$user", "$password", "$database");
$query ="SELECT * FROM property ORDER BY ID DESC";
$result = mysqli_query($connect, $query);

?>
<!DOCTYPE html>
<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

</head>
<body>
<br /><br />

<section>
    <h3>Search for a property </h3>
    <form  method="get" action="./propertySearch.php" id="searchform">

            <tr>
                <td> Suburb </td>
                <td> <input name="search_suburb" type="text" id="search_suburb" placeholder="Clayton"> </td>
                <td> Type </td>
                <td> <input name="search_type" type="text" id="search_type" placeholder="Apartment"> </td>
                <td> <input  type="submit" name="submit" value="Search"> </td>
            </tr>

    </form>
</section>

<div class="container">
    <h3 align="center">Ruthless Real Estate Properties</h3>
    <br />
    <div class="table-responsive">
        <table id="property_data" class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Client</td>
                <td>Property street</td>
                <td>Suburb</td>
                <td>Property State</td>
                <td>Postcode</td>
                <td>List Price</td>
                <td>List Date</td>
                <td>Sale Date</td>
                <td>Sale Price</td>
                <td>Property Type</td>
                <td>Image</td>
                <td>Description</td>
                <td>Actions</td>
            </tr>
            </thead>
            <?php
            while($row = mysqli_fetch_array($result))
            {
               ?>

                               <tr>  
                                    <td> <?php echo $row["id"]  ?> </td>
                                    <td><?php echo$row["client_id"] ?> </td>
                                    <td> <?php echo$row["property_street"] ?></td>
                                    <td> <?php echo $row["property_suburb"]?></td>
                                   <td><?php echo $row["property_state"]?></td>
                                    <td><?php echo $row["property_pc"]?></td>
                                    <td><?php echo $row["list_price"]?></td>
                                    <td><?php echo $row["list_date"]?></td>
                                    <td><?php echo $row["sale_date"]?></td>
                                    <td><?php echo $row["sales_price"]?> </td>
                                    <td><?php echo $row["property_type"]?></td>
                                    
                                    <?php $image = "property_images/".$row["image_name"];?>
		
		                            <td> <img src="<?php echo $image?>" width="128px" height="128px"> </td>
                                   
                                    <td><?php echo $row["description"]?> </td>
                                    <td>

                                     <a href="propertyModify.php?Action=Update&id=<?php echo $row["id"];?>">Update</a>
                                      <a href="propertyModify.php?Action=Delete&id=<?php echo $row["id"];?>">Delete</a>
                                         </td>
                               </tr>  

         <?php
            }
            ?>
        </table>
    </div>
</div>
</body>
</html>
<footer>
    <center>
        <a   target='_blank' href="./displayCode.php?name=view_property.php"><img width="500" height="50" src="./images/property.PNG"></a>
    </center>
</footer>
<script>
    $(document).ready(function(){
        $('#property_data').DataTable();
    });
</script>