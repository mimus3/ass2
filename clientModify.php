<?php include './navbar.php';?>
<?php
ob_start();
?>

<html>
<head><title></title></head>
<link rel="stylesheet" type="text/css" href="style.css">
<body>
<script language="javascript">
    function confirm_delete()
    {
        window.location='clientModify.php?id=<?php echo $_GET["id"]; ?>&Action=ConfirmDelete';
    }
</script>
<center><h3>Client Modification</h3></center>
<?php
include("connection.php");
$connect = new mysqli($host, $user, $password, $database)
or die("Couldn't log on to database");

$query = "SELECT * FROM client WHERE id =".$_GET["id"];
$result = $connect->query(($query));
$row = $result->fetch_assoc();

$strAction = $_GET["Action"];

switch($strAction)
{
case "Update":
    ?>
    <form method="post" action="clientModify.php?id=<?php echo $_GET["id"]; ?>&Action=ConfirmUpdate">
        <center>client details amendment<br /></center><p />
        <table align="center" cellpadding="3">
            <tr />
            <td><b>Client ID</b></td>
            <td><?php echo $row["id"]; ?></td>
            </tr>
            <tr>
                <td><b>Given Name</b></td>
                <td><input type="text" name="givenname" size="30" value="<?php echo $row["client_gname"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Family Name</b></td>
                <td><input type="text" name="familyname" size="40" value="<?php echo $row["client_fname"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Client Street</b></td>
                <td><input type="text" name="clientstreet" size="10" value="<?php echo $row["client_street"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Client Suburb</b></td>
                <td><input type="text" name="clientsuburb" size="10" value="<?php echo $row["client_suburb"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Client State</b></td>
                <td><select name="client_state">
                        <option></option>
                        <option>VIC</option>
                        <option>SA</option>
                        <option>TAS</option>
                        <option>WA</option>
                        <option>QLD</option>
                        <option>ACT</option>
                        <option>NSW</option>
                        <option>NT</option>
                    </select></td> </tr>
            <tr>
                <td><b>Client Postcode</b></td>
                <td><input type="text" name="clientpostcode" size="10" value="<?php echo $row["client_pc"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Client Email</b></td>
                <td><input type="text" name="clientemail" size="10" value="<?php echo $row["client_email"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Mobile Phone</b></td>
                <td><input type="text" name="mobilephone" size="10" value="<?php echo $row["client_mobile"]; ?>"></td>
            </tr>
            <tr>
                <td><b>Mailing List</b></td>
                <td><select name="client_mailinglist">
                        <option></option>
                        <option>1</option>
                        <option>0</option>
                    </select>
                </td> </tr>
        </table>
        <br/>
        <table align="center">
            <tr>
                <td><input type="submit" value="Update client"></td>
                <td><input type="button" value="Return to List" OnClick="window.location='view_client.php'"></td>
            </tr>
        </table>
    </form>
    <?php
    break;

case "ConfirmUpdate":
    {
        $query= "UPDATE client set client_gname ='$_POST[givenname]',
        client_fname ='$_POST[familyname]',client_street ='$_POST[clientstreet]',
        client_suburb ='$_POST[clientsuburb]',client_state ='$_POST[clientstate]',
        client_pc ='$_POST[clientpostcode]',client_email ='$_POST[clientemail]',
        client_mobile ='$_POST[mobilephone]',client_mailinglist = '$_POST[mailinglist]'
	                 WHERE id =" .$_GET["id"];
if ($connect->query($query))
{
?>
<center>
    The following client record has been successfully update<p/>
    <?php
    echo "Client No. $row[id] ";
    echo "</center><p />";
    echo "<center><a href='view_client.php'><h4 class='alert_success'>The client details has successfully added</h4></a></center>";

    }


    else {
        echo "<center>Error updatinng property record<p /></center>";
    }
    }
    break;



case "Delete":
    ?>
    <center>Confirm deletion of the following Client record<br /></center><p />
    <table align="center" cellpadding="3">
        <tr />
        <td><b>Client ID</b></td>
        <td><?php echo $row["id"]; ?></td>
        </tr>
        <tr>
            <td><b>Client Given Name</b></td>
            <td><?php echo $row["client_gname"]; ?></td>
        </tr>
        <tr>
            <td><b>Client Family Name</b></td>
            <td><?php echo $row["client_fname"]; ?></td>
        </tr>
        <tr>
            <td><b>Street </b></td>
            <td><?php echo $row["client_street"]; ?></td>
        </tr>
        <tr>
            <td><b>Suburb</b></td>
            <td><?php echo $row["client_suburb"]; ?></td>
        </tr>
        <tr>
            <td><b>State</b></td>
            <td><?php echo $row["client_state"]; ?></td>
        </tr>
        <tr>
            <td><b>Post Code</b></td>
            <td><?php echo $row["client_pc"]; ?></td>
        </tr>

    </table>
    <br/>
    <table align="center">
        <tr>
            <td><input type="button" value="Confirm" OnClick="confirm_delete();">
            <td><input type="button" value="Cancel" OnClick="window.location='view_client.php'"></td>
        </tr>
    </table>
    <?php
    break;

case "ConfirmDelete":
$query="DELETE FROM client WHERE id =".$_GET["id"]

;
if($connect->query($query))
{
?>
<center>
    The following client record has been successfully deleted<p />
    <?php
    echo " Client $row[id] ";
    echo "</center><p />";
    }
    else
    {
        echo "<center>Error deleting client record<p /></center>";
    }
    echo "<center><input type='button' value='Return to List' OnClick='window.location=\"view_client.php\"'></center>";
    break;
    }
    $result->free_result();
    $connect->close();
    ?>

</body>
</html>













