<?php include './logincheck.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
</head>
<?php include './navbar.php';?>
<body>
<table border="0" style="border-style:double">
    <center><h1>Property Client Details</h1></center>
    <form method="post" action="client_success.php">
        <tr>
            <td width="73">Given Name:</td>
            <td width="169"><input type="text" name="client_gname" size="35" maxlength="50" placeholder="David" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('Given Name Required. Please enter your Given Name')" required autofocus></td>
            <td width="73">First Name:</td>
            <td width="169"><input type="text" name="client_fname" size="35" maxlength="50" placeholder="John" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('First Name required. Please enter your First name.')" required autofocus></td>
        </tr>
        <tr>
            <td>Street Address:</td>
            <td><input type="text" name="client_street" size="35" maxlength="50" placeholder="21 Clayton Road" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('your address is required, please put a valid street address.')" required autofocus></td>
            <td>Suburb:</td>
            <td><input type="text" name="client_suburb" size="35" maxlength="50" placeholder="Clayton" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('The suburb character is longer .')" > </td>
        </tr>
        <tr>
            <td>State:</td>
            <td><select name="client_state">
                    <option></option>
                    <option>VIC</option>
                    <option>SA</option>
                    <option>TAS</option>
                    <option>WA</option>
                    <option>QLD</option>
                    <option>ACT</option>
                    <option>NSW</option>
                    <option>NT</option>
                </select></td>
            <td>Postcode:</td>
            <td><input type="number" name="client_pc" size=4 min="0" max="9999" maxlength="4" placeholder="3000"  onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('Postcode cannot be longer than 4 numbers')" ></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><input type="text" name="client_email" size="35" maxlength="50" placeholder="john.doe@gmail.com")></td>

            <td>Mobile:</td>
            <td><input type="text" name="client_mobile" size="10"  pattern="[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" max="9999999999" placeholder="e.g. 0430000000" value="0" onInput="setCustomValidity('')" onInvalid="this.setCustomValidity('Mobile Numbers cannot be longer than 10 numbers.')"></td>
        </tr>
        <tr>
            <td>Mailing list:</td>
            <td><select name="client_mailinglist">
                    <option></option>
                    <option>1</option>
                    <option>0</option>
                </select>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="Submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="Reset"> <input type="submit" OnClick='window.history.back()'value="Back"</></td>
        </tr>

    </form>
</table>


</body>
</html>