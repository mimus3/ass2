<!DOCTYPE html>
<html>

<head>
    <meta charset='Cp1252'>

    <title>Responsive Table</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font: 14px/1.4 Georgia, Serif;
        }


        table {
            width: 100%;
            border-collapse: collapse;
        }

        tr:nth-of-type(odd) {
            background: #eee;
        }
        th {
            background: #333;
            color: white;
            font-weight: bold;
        }
        td, th {
            padding: 6px;
            border: 1px solid #9B9B9B;
            text-align: left;
        }
        @media
        only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px)  {
            table, thead, tbody, th, td, tr { display: block; }
            thead tr { position: absolute;top: -9999px;left: -9999px;}
            tr { border: 1px solid #9B9B9B; }
            td { border: none;border-bottom: 1px solid #9B9B9B; position: relative;padding-left: 50%; }

            td:before { position: absolute;top: 6px;left: 6px;width: 45%; padding-right: 10px; white-space: nowrap;}

            /*
            client table
            */
            td:nth-of-type(0):before { content: "Id"; }
            td:nth-of-type(1):before { content: "Client Given Name"; }
            td:nth-of-type(2):before { content: "Client First Name"; }
            td:nth-of-type(3):before { content: "Client Street"; }
            td:nth-of-type(4):before { content: "Client Suburb"; }
            td:nth-of-type(5):before { content: "Client State"; }
            td:nth-of-type(6):before { content: "Client Poscode"; }
            td:nth-of-type(7):before { content: "Client Email"; }
            td:nth-of-type(8):before { content: "Client Mobile"; }
            td:nth-of-type(9):before { content: "client Mailing List"; }
        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px; }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

    </style>
    <!--<![endif]-->

</head>

<body>
<center><h1>Test Data</h1></center>
<h1>Client Table</h1>
<table><thead><tr>	<th>Id</th>
        <th>Client Given Name</th>
        <th>Client First Name</th>
        <th>Client Street</th>
        <th>Client Suburb</th>
        <th>Client State</th>
        <th>Client Postcode</th>
        <th>Client Email</th>
        <th>Client Mobile</th>
        <th>Client Mailing List</th>
    </tr></thead>
    <tbody id="data">

    <tr>
        <td align="right">399</td>
        <td>Brian</td>
        <td>Nathan</td>
        <td>Ap #139-2591 tellus Av.</td>
        <td>joondalup</td>
        <td>VIC</td>
        <td align="right">8346</td>
        <td>venenatis@nisi.ca</td>
        <td>(01)60465954</td>
        <td>0</td>
    </tr>
    <tr>
        <td align="right">398</td>
        <td>Aurora</td>
        <td>Hayes</td>
        <td>Ap #724-3620 Nullam Street</td>
        <td>Charters Towers</td>
        <td>QLD</td>
        <td align="right">8402</td>
        <td>dis@nunc.org</td>
        <td>(02)47220824</td>
        <td>0</td>
    </tr>
    <tr>
        <td align="right">397</td>
        <td>Wanda</td>
        <td>Deacon</td>
        <td>2861 Mollis. RD.</td>
        <td>Parramatta</td>
        <td>NSW</td>
        <td align="right">8546</td>
        <td>scelerisque.lorem@Duiselementum.org</td>
        <td>(07)9332 6285</td>
        <td>1</td>
    </tr>
    <tr>
        <td align="right">396</td>
        <td>Kyla</td>
        <td>Merritt</td>
        <td>258-8799 Ante St.</td>
        <td>Kalgorlie-Boulder</td>
        <td>WA</td>
        <td align="right">2049</td>
        <td>sed.eu.eros@Nullasemperlluss.co.uk</td>
        <td>(09)5413 2681</td>
        <td>0</td>
    </tr>
    <tr>
        <td align="right">395</td>
        <td>David</td>
        <td>Dillon</td>
        <td>251-7054 tempus Avenue</td>
        <td>Adelaide</td>
        <td>SA</td>
        <td align="right">5697</td>
        <td>metus@duiSuspendisse.edu</td>
        <td>(04)8833 3047</td>
        <td>1</td>
    </tr>
    <tr>
        <td align="right">394</td>
        <td>Jocelyn</td>
        <td>Cooper </td>
        <td>944-6138 Elit, Av.</td>
        <td>Wollongong</td>
        <td>NSW</td>
        <td align="right">2540</td>
        <td>lacinia.orci@loborituices.edu</td>
        <td>(07)9332 6285</td>
        <td>1</td>
    </tr>



    </tbody>
</table>
</body>
</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset='Cp1252'>

    <title>Responsive Table</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font: 14px/1.4 Georgia, Serif;
        }

        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }
        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }
        th {
            background: #333;
            color: white;
            font-weight: bold;
        }
        td, th {
            padding: 6px;
            border: 1px solid #9B9B9B;
            text-align: left;
        }
        @media
        only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px)  {
            table, thead, tbody, th, td, tr { display: block; }
            thead tr { position: absolute;top: -9999px;left: -9999px;}
            tr { border: 1px solid #9B9B9B; }
            td { border: none;border-bottom: 1px solid #9B9B9B; position: relative;padding-left: 50%; }

            td:before { position: absolute;top: 6px;left: 6px;width: 45%; padding-right: 10px; white-space: nowrap;}

            /*
            Label the data
            */
            td:nth-of-type(0):before { content: "FEATURE_ID"; }
            td:nth-of-type(1):before { content: "FEATURE_NAME"; }
        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px; }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

    </style>
    <!--<![endif]-->

</head>

<body>
<h1>Feature Table</h1>
<table><thead><tr>	<th>Id</th>
        <th>Feature Name</th>
    </tr></thead>
    <tbody id="data">

    <tr>
        <td align="right">1</td>
        <td>Swimming Pool</td>
    </tr>
    <tr>
        <td align="right">2</td>
        <td>Parking</td>
    </tr>

    <tr>
        <td align="right">3</td>
        <td>Tennis Court</td>
    </tr>
    <tr>
        <td align="right">4</td>
        <td>Wine Cellar</td>
    </tr>
    <tr>
        <td align="right">5</td>
        <td>Helipad</td>
    </tr>
    <tr>
        <td align="right">6</td>
        <td>Showroom</td>
    </tr>
    <tr>
        <td align="right">10</td>
        <td>Media Room</td>
    </tr>
    <tr>
        <td align="right">11</td>
        <td>Basement</td>
    </tr>
    <tr>
        <td align="right">12</td>
        <td>Home Theater</td>
    </tr>
    <tr>
        <td align="right">13</td>
        <td>Library</td>
    </tr>

    </tbody></table>
</body>
</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset='Cp1252'>

    <title>Responsive Table</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font: 14px/1.4 Georgia, Serif;
        }

        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }
        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }
        th {
            background: #333;
            color: white;
            font-weight: bold;
        }
        td, th {
            padding: 6px;
            border: 1px solid #9B9B9B;
            text-align: left;
        }
        @media
        only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px)  {
            table, thead, tbody, th, td, tr { display: block; }
            thead tr { position: absolute;top: -9999px;left: -9999px;}
            tr { border: 1px solid #9B9B9B; }
            td { border: none;border-bottom: 1px solid #9B9B9B; position: relative;padding-left: 50%; }

            td:before { position: absolute;top: 6px;left: 6px;width: 45%; padding-right: 10px; white-space: nowrap;}

            /*
            Label the data
            */
            td:nth-of-type(0):before { content: "PROPERTY_ID"; }
            td:nth-of-type(1):before { content: "PROPERTY_STREET"; }
            td:nth-of-type(2):before { content: "PROPERTY_SUBURB"; }
            td:nth-of-type(3):before { content: "PROPERTY_STATE"; }
            td:nth-of-type(4):before { content: "PROPERTY_PC"; }
            td:nth-of-type(5):before { content: "PROPERTY_TYPE"; }
        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px; }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

    </style>
    <!--<![endif]-->

</head>

<body>
<H1>Property Table</H1>
<table><thead><tr>	<th>Id</th>
        <th>Client Id</th>
        <th>Property Street</th>
        <th>Property Suburb</th>
        <th>Property State</th>
        <th>Property Postcode</th>
        <th>List Price</th>
        <th>List Date</th>
        <th>Sale Date</th>
        <th>Sales Price</th>
        <th>Property Type</th>
        <th>Image path name</th>
        <th>Description</th>
    </tr></thead>
    <tbody id="data">

    <tr>
        <td align="right">100</td>
        <td>399</td>
        <td>40 Windella Cress</td>
        <td>Glen Waverley</td>
        <td>WA</td>
        <td>3150</td>
        <td align="right">54</td>
        <td align="right">28/09/2018</td>
        <td align="right">30/09/2018</td>
        <td align="right">350,000</td>
        <td>Apartment</td>
        <td>house1.jpg</td>
        <td>1234</td>
    </tr>



    <tr>
        <td align="right">120</td>
        <td>398</td>
        <td>431 Mauris Road</td>
        <td>Lithgow</td>
        <td>NSW</td>
        <td>9779</td>
        <td align="right">1234</td>
        <td align="right">24/09/2018</td>
        <td align="right">30/09/2018</td>
        <td align="right">601,89</td>
        <td>Houses</td>
        <td>house2.jpg</td>
        <td>Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere</td>
    </tr>

    <tr>
        <td align="right">176</td>
        <td>395</td>
        <td>21 monash</td>
        <td>clayton</td>
        <td>VIC</td>
        <td>3000</td>
        <td align="right">14678</td>
        <td align="right">28/10/2018</td>
        <td align="right"></td>
        <td align="right"></td>
        <td>Town House</td>
        <td>house3.jpg</td>
        <td>new house</td>
    </tr>

    <tr>
        <td align="right">177</td>
        <td>394</td>
        <td>4 John street</td>
        <td>caulfield</td>
        <td>VIC</td>
        <td>3016</td>
        <td align="right">500000</td>
        <td align="right">28/10/2018</td>
        <td align="right">16/10/2018</td>
        <td align="right">789754</td>
        <td>Apartment</td>
        <td>house2.jpg</td>
        <td>Brand new house</td>
    </tr>

    <tr>
        <td align="right">178</td>
        <td>299</td>
        <td>6 Gilbert road</td>
        <td>Dandenong</td>
        <td>VIC</td>
        <td>5678</td>
        <td align="right">456789</td>
        <td align="right">28/10/2018</td>
        <td align="right"></td>
        <td align="right"></td>
        <td>Cabin</td>
        <td>house2.jpg</td>
        <td>Brand new house</td>
    </tr>

    <tr>
        <td align="right">179</td>
        <td>297</td>
        <td>1 Elizabeth street</td>
        <td>Carlton</td>
        <td>NSW</td>
        <td>5678</td>
        <td align="right">43215</td>
        <td align="right">28/10/2018</td>
        <td align="right"></td>
        <td align="right"></td>
        <td>Mansion</td>
        <td>house3.jpg</td>
        <td>Brand new house</td>
    </tr>



    </tbody>
</table>
</body>
</html>


<head>
    <meta charset='Cp1252'>

    <title>Responsive Table</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body {
            font: 14px/1.4 Georgia, Serif;
        }

        /*
        Generic Styling, for Desktops/Laptops
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }
        /* Zebra striping */
        tr:nth-of-type(odd) {
            background: #eee;
        }
        th {
            background: #333;
            color: white;
            font-weight: bold;
        }
        td, th {
            padding: 6px;
            border: 1px solid #9B9B9B;
            text-align: left;
        }
        @media
        only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px)  {
            table, thead, tbody, th, td, tr { display: block; }
            thead tr { position: absolute;top: -9999px;left: -9999px;}
            tr { border: 1px solid #9B9B9B; }
            td { border: none;border-bottom: 1px solid #9B9B9B; position: relative;padding-left: 50%; }

            td:before { position: absolute;top: 6px;left: 6px;width: 45%; padding-right: 10px; white-space: nowrap;}

            /*
            Label the data
            */
            td:nth-of-type(0):before { content: "TYPE_ID"; }
            td:nth-of-type(1):before { content: "TYPE_NAME"; }
        }

        /* Smartphones (portrait and landscape) ----------- */
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
            body {
                padding: 0;
                margin: 0;
                width: 320px; }
        }

        /* iPads (portrait and landscape) ----------- */
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            body {
                width: 495px;
            }
        }

    </style>
    <!--<![endif]-->


</head>

<body>
<h1>Type Table</h1>
<table><thead><tr>	<th>Id</th>
        <th>Type Name</th>
    </tr></thead>
    <tbody id="data">

    <tr>
        <td align="right">1</td>
        <td>House</td>
    </tr>
    <tr>
        <td align="right">2</td>
        <td>Apartment</td>
    </tr>
    <tr>
        <td align="right">3</td>
        <td>Shop</td>
    </tr>
    <tr>
        <td align="right">4</td>
        <td>Factory</td>
    </tr>
    <tr>
        <td align="right">5</td>
        <td>Warehouse</td>
    </tr>
    <tr>
        <td align="right">202</td>
        <td>Cabin</td>
    </tr>
    <tr>
        <td align="right">203</td>
        <td>Town House</td>
    </tr>
    <tr>
        <td align="right">204</td>
        <td>Mansion</td>
    </tr>
    <tr>
        <td align="right">205</td>
        <td>Office</td>
    </tr>
    <tr>
        <td align="right">206</td>
        <td>Healthcare</td>
    </tr>


    </tbody>
</table>
</body>
</html>
