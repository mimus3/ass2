<?php
include "connection.php"; ?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <title>Dashboard I Admin Panel</title>

    <link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
    <style type="text/css">
        #main .module.width_full .module_content .stats_graph center table tr td table {
            font-size: 10px;
        }
    </style>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="js/hideshow.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.equalHeight.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
            {
                $(".tablesorter").tablesorter();
            }
        );
        $(document).ready(function() {

            //When page loads...
            $(".tab_content").hide(); //Hide all content
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content

            //On Click Event
            $("ul.tabs li").click(function() {

                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(this).addClass("active"); //Add "active" class to selected tab
                $(".tab_content").hide(); //Hide all tab content

                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                $(activeTab).fadeIn(); //Fade in the active ID content
                return false;
            });

        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('.column').equalHeight();
        });
    </script>

</head>


<body>

<header id="header">
    <hgroup>
        <h1 class="site_title"><a href="index.php">Ruthless Real Estate</a></h1>
        <h2 class="section_title">Dashboard</h2>
    </hgroup>
</header> <!-- end of header bar -->

<section id="secondary_bar">
    <div class="user">
        <p><?php $_SESSION['myusername']; ?> (<a href="index.php">Admin</a>)</p>
        <!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
    </div>
    <div class="breadcrumbs_container">
        <article class="breadcrumbs"><a href="index.html">Website Admin</a> <div class="breadcrumb_divider"></div>
            <a class="current">Admin</a></article>
    </div>
</section><!-- end of secondary bar -->

<aside id="sidebar" class="column">
    <h3><a href="index.php">Home</a></h3>

    <h3>Property</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><a href="view_property.php">View Property</a></li>
        <li class="icn_new_article"><a href="add_property.php">Add New Property</a></li>

    </ul>

    <h3>Client</h3>
    <ul class="toggle">
        <li class="icn_profile"><a href="view_client.php">View client</a></li>
        <li class="icn_add_user"><a href="add_client.php">Add Client</a></li>
    </ul>

    <h3>Property Type</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><a href="view_propertytype.php">View Property Type</a></li>
        <li class="icn_new_article"><a href="add_propertytype.php">Add New Property Type</a></li>
    </ul>

    <h3>Feature</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><a href="view_propertyfeature.php">View Feature</a></li>
        <li class="icn_new_article"><a href="add_propertyfeature.php">Add New Feature</a></li>
    </ul>


    <h3>Multiple Properties</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><a href="mutipleproperties.php">View Multiple Properties</a></li>
    </ul>

    <h3>Images</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><a href="view_image.php">View Property Image</a></li>
    </ul>

    <h3>Documentation</h3>
    <ul class="toggle">
        <li class="icn_edit_article"><a href="documentation.php">View Documentation</a></li>
    </ul>

    <h3>Admin</h3>
    <ul class="toggle">
        <li class="icn_jump_back"><a href="logout.php">Logout</a></li>
    </ul>

    <footer>
        <hr />
        <p><strong>Copyright &copy; 2018 Ruthless Real Estate</strong></p>
        <p>&nbsp;</p>
    </footer>
</aside><!-- end of sidebar -->



</body>


</html>

