<!DOCTYPE html>

<html lang="en">
<?php include './bootstrap.php';?>
<div class="slideshow-container">
    <link href="../css/slideshow.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="./jQuery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="./jQuery/slideshow.js"></script>


    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .container {
                position: relative;
                text-align: center;
                color: white;
            }

            .bottom-left {
                position: absolute;
                bottom: 8px;
                left: 16px;
            }

            .top-left {
                position: absolute;
                top: 8px;
                left: 16px;
            }

            .top-right {
                position: absolute;
                top: 8px;
                right: 16px;
            }

            .bottom-right {
                position: absolute;
                bottom: 8px;
                right: 16px;
            }

            .centered {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
        </style>


    </head>



    <body>

    <div class="container">
        <img src="./images/house2.jpg" alt="House" width="1350" height="500">
        <div class="centered"><h1>Welcome to Ruthless real estate </h1></div>
    </div>

    <h1>LATEST NEWS </h1>
    <hr>
    <div class="container">
        <img src="./images/house4.jpeg" alt="Nature" width="350" height="300">
        <div class="bottom-right">The latest property to hit the market</div>

        <img src="./images/house5.jpg" alt="Nature" width="350" height="300">
        <div class="bottom-left">Bottom Left</div>
        <img src="./images/house6.jpeg" alt="Nature" width="350" height="300">
        <div class="bottom-left">Bottom Left</div>

    </div>
    <hr>

    <h1>NEW HOMES</h1>
    <div class="container">
        <img src="./images/house1.jpg" alt="house1" width="350" height="333">
        <img src="./images/house2.jpg" alt="house2" width="350" height="333">
        <img src="./images/house3.jpg" alt="house3" width="350" height="333">
    </div>

    </body>

    <footer>
        <p>Posted by: Mohammed Imam Mustapha</p>
        <p>Contact information: <a href="mailto:customerservice@ruthlessrealestate.com">customerservice@ruthlessrealestate.com</a>.</p>
    </footer>


</html>
