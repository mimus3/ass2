
<?php
session_start();


include("connection.php");
$connect = new mysqli($host, $user, $password, $database);
$query="select * from client ORDER BY client_gname asc ";
$result = mysqli_query($connect,$query);


require ("clientPDF/fpdf.php");
$pdf=new FPDF('p','mm','A3');
 $pdf->AddPage();
$pdf->SetFont("Arial","B",20);
$pdf->Cell(273,10,"Ruthless Estate Client",1,1,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(10,10,"ID",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(22,10,"Given Name",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(22,10,"Family Name",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(30,10,"Street",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(30,10,"Suburb",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(22,10,"State",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(22,10,"Postcode",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(55,10,"Email",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(35,10,"Mobile",1,0,"C");
$pdf->SetFont("Arial","",10);
$pdf->Cell(25,10,"Mailinglist",1,1,"C");

while ($row=mysqli_fetch_array($result)){

    $pdf->SetFont("Arial","",10);
    $pdf->Cell(10,10,$row['id'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(22,10,$row['client_gname'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(22,10,$row['client_fname'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(30,10,$row['client_street'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(30,10,$row['client_suburb'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(22,10,$row['client_state'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(22,10,$row['client_pc'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(55,10,$row['client_email'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(35,10,$row['client_mobile'],1,0);
    $pdf->SetFont("Arial","",10);
    $pdf->Cell(25,10,$row['client_mailinglist'],1,1);
}

 $pdf->Output();


