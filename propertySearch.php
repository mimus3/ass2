<!DOCTYPE html>
<html lang="en">
<?php include './bootstrap.php';?>
<?php require './navbar.php';?>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

</head>
<div class="container">
    <h1>Property Search</h1>

    <form  method="get" action="./propertySearch.php" id="searchform">

            <tr>
                <td> Suburb </td>
                <td> <input name="search_suburb" type="text" id="search_suburb" placeholder="Clayton"> </td>
                <td> Type </td>
                <td> <input name="search_type" type="text" id="search_type" placeholder="Apartment"> </td>
                <td> <input  type="submit" name="submit" value="Search"> </td>
            </tr>

    </form>
    <?php
    require './connection.php';
    $connect = mysqli_connect("$host", "$user", "$password", "$database");
    $query;


    if(empty($_GET["search_suburb"])) //searching for type
    {
        $query = "SELECT * FROM property WHERE (property_type LIKE '%".strtoupper($_GET["search_type"])."%')";
    }
    else if (empty($_GET["search_type"])) //searching for suburb
    {
        $query = "SELECT * FROM property WHERE (property_suburb LIKE '%".strtoupper($_GET["search_suburb"])."%')";
    }
    else //searching both property type and suburb
    {
        $query = "SELECT * FROM property WHERE (property_type LIKE '%".strtoupper($_GET["search_type"])."%'
AND property_suburb LIKE '%".strtoupper($_GET["search_suburb"])."%' )";
    }
    $result = mysqli_query($connect, $query);

    if ($result){
        ?>
    <div class="table-responsive">
        <table id="property_data" class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Id</td>
                <td>Client</td>
                <td>Property street</td>
                <td>Suburb</td>
                <td>Postcode</td>
                <td>List Price</td>
                <td>List Date</td>
                <td>Sale Date</td>
                <td>Sale Price</td>
                <td>Property Type</td>
                <td>Image</td>
                <td>Description</td>
                <td>Actions</td>
            </tr>
            </thead>

            <?php
            while ($row = mysqli_fetch_array($result)){
                ?>
                <tr>
                    <td> <?php echo $row["id"]  ?> </td>
                    <td><?php echo$row["client_id"] ?> </td>
                    <td> <?php echo$row["property_street"] ?></td>
                    <td> <?php echo $row["property_suburb"]?></td>
                    <td><?php echo $row["property_pc"]?></td>
                    <td><?php echo $row["list_price"]?></td>
                    <td><?php echo $row["list_date"]?></td>
                    <td><?php echo $row["sale_date"]?></td>
                    <td><?php echo $row["sales_price"]?> </td>
                    <td><?php echo $row["property_type"]?></td>

                    <?php $image = "property_images/".$row["image_name"];?>

                    <td> <img src="<?php echo $image?>" width="128px" height="128px"> </td>

                    <td><?php echo $row["description"]?> </td>
                    <td>

                        <a href="propertyModify.php?Action=Update&id=<?php echo $row["id"];?>">Update</a>
                        <a href="propertyModify.php?Action=Delete&id=<?php echo $row["id"];?>">Delete</a>
                    </td>
                </tr>
                <?php
            }

            ?>

        </table>
        <?php
    } else {
    }
    ?>
</div>
</html>
